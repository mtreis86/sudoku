(in-package :sudoku)

#| *** General theory, plan for building - written before starting. ***

First I will write a quick function for testing to confirm that a puzzle is
solved, then a couple helper functions I know I will need:
- Solved: this will both check that all points have an entry, and that no
row/col/box has more than one unique entry.
- New: Initialize all objects.
- Load: Load in a puzzle from file.
- Update: Will update main array and vectors when a solution is found.
- Printer: Will display the array in a human-friendly manner including some
sort of separator between boxes.

Second I will write the easy solve functions:
- But-one: this will check row/col/box for any containing 8 entries and add the
one that is missing. Since I will have a vector for each of those possible
location, this function will just check the vectors.
- Only-one: Since I will be maintaing an array of possibility vectors, this
function will effectively just scan for vectors of one entry remaining that
have not yet been added to the main array.

Third will be medium difficulty solutions:
- No-choice: This will check for cases where where only one number can fit in a
entry, usually forced due to other rows/cols having that number already and only
one square being left in that box.

Last will be difficult solutions:
- Prioritize: This will will be a sorting function that organizes the remaining
entries by how many other entries get solved if a particular solution is used in
that one.
- Guess: This will take entries on the priority list and see if by picking one,
can the rest of the puzzle be solved. If picking one leads to a situation where
it definately doesn't work then that number can be eliminated as a choice.
- Matching: This works on the guess function, looking for matching arrays to
explore. If two guesses result in the same patterns being generated, it is more
likely they are correct, so best to walk that path first.

Organization:
I want to make this dimension agnostic, but for the start will be targeting 2d
9x9 arrays. If it is obvious to me that something can be done in an agnostic way
without mental overhead I will pick that. Otherwise I will come back and add
multi-dimensions later.
I will need vectors representing array indexes to count the number of entries.
I will need some arrays:
- Main: hold definite solutions.
- Possibles: holds potentially valid choices.
- Tests: holds guesses.

I may have the tests arrays be an array-of-arrays where each guess holds both
the guess and the resulting array after running all low/med solvers on it.

I may have a mix of breadth and depth solvers by using the prioritization list
to pick a few entries to fully explore before moving to a wider pattern.

I will obviously need a way to load in sudoku puzzles to test, and preferably a
way to load in solutions. I found a website with a csv generator so for now I
will use that, and can write a function to read those in from a file and convert
them into the array. For now I will read simple non-csv files that consist of
only one line, with only numbers and dots in it, as a row-major format to fill
the array directly from.

Once this all works I will think about how to interact with it.

Then I will modify the above to work as a generator of problems as well.

For now the way I want to print the arrays should be something like:
------   ---
| 1 2 3 | 4 .
| 4 . 5 | . .
| . . 9 | . .
------   ---
| 3 . . | . .
| . . . | . .

etc. With dashes as vertical separators, pipes as horizontal separators, dots as
empty entries, and spaces between things to make it easy to read.

While I will display the empty entries as dots for human readability, I will
have them be zeros in the array, to make the array of a type that can be
directly in memeory and not require pointers.

Since this will be mostly passing around alists and working on a set of arrays
with various functions, for now I will make this a structure. I may use CLOS
instead, will see how it goes. Maybe both for practice. I think structure will
be sufficient, but CLOS may be better for multidimensional puzzles as I can
more easily modify the original code for that purpose, as well as implementing
a generator. Perhaps the best way for practice purposes will be to write it as
a structure for now, then convert to CLOS, then add dimensions and the
generator...

Since I will need some sort of feedback into the main loop as to whether the
solver functions found anything, I can either have the solvers return multiple
values, or I can have the update-the-vectors function return multiple values.
Or both. Both probably will work better as I can then have a main loop that
terminates when vector doesn't update with a number of smaller loops in it that
terminate when those functions stop updating.


*** Notes for issues, written after building. ***

There is an edge case I am not covering yet where its maybe possible, however
unlikely that the guesses would fail. I only run the determinitive steps after
a guess, but probably should have guesses also be part of that process.

I want to make this 3d and am working on a CLOS version of this that is
agnostic to the dimensions so I may not fix this library itself.

A lot of the code here is accessing arrays in the same manner with slight
differences so I can probably write an array library that would give me
more power to do that without having so much repetition.

I should probably run the tests for whether a sudoku is solved at the end of
the process rather than just assume that because the process finished that it
actually worked, and have it pop an error if not. Then run a series of puzzles
through it to confirm that it never fails to find a true answer.

I need to write both unit and integration tests for all the functions.

Since the arrays created are rather small, I can probably write safer code
that makes copies of the array at each function rather than working on the
original array. That would make it a bit easier to track issues if it turns
out something in the code is wrong. Also will make testing easier.

I should probably make these notes into github issues.

At some point I should rewrite the above description of operation into a
documentation text with the theory of operation. I ended up combining two
of the four steps together within another step, so the end result was three
main steps, one of which is split into two sub steps.|#

(defstruct (sudoku (:print-function print-sudoku))
  main size box-size)

(defstruct (solver (:print-function print-solver)
                   (:include sudoku))
  rows cols boxs)

(defun solvedp (sudoku)
  "Has this puzzle been solved?"
  (and (test-rows sudoku)
       (test-cols sudoku)
       (test-boxs sudoku)))

(defun valid-entry (entry size)
  (and (> entry 0)
       (<= entry size)))

(defun test-rows (sudoku)
  "Are all the rows complete and of unique entries?"
  (let ((size (array-dimension (sudoku-main sudoku) 0))
        (count 0))
    (loop for row from 0 below size do
      (loop with founds = nil
            for col from 0 below size do
              (let ((entry (aref (sudoku-main sudoku) row col)))
                (if (not (member entry founds))
                    (push entry founds)
                    (progn
                      (format t "Error found at (~A ~A)" row col)
                      (print-sudoku sudoku)
                      (return-from test-rows nil)))
                (when (valid-entry entry (sudoku-size sudoku))
                  (incf count)))))
    (= count (expt size 2))))

(defun test-cols (sudoku)
  "Are all the columns complete and of unique entries?"
  (let ((size (array-dimension (sudoku-main sudoku) 0))
        (count 0))
    (loop for col from 0 below size do
      (loop with founds = nil
            for row from 0 below size do
              (let ((entry (aref (sudoku-main sudoku) row col)))
                (if (not (member entry founds))
                    (push entry founds)
                    (progn
                      (format t "Error found at (~A ~A)" row col)
                      (print-sudoku sudoku)
                      (return-from test-cols nil)))
                (when (valid-entry entry (sudoku-size sudoku))
                  (incf count)))))
    (= count (expt size 2))))

(defun test-boxs (sudoku)
  "Are all the boxes complete and of unique entries?"
  (let ((size (sudoku-box-size sudoku))
        (count 0))
    (loop for box-row from 0 below size do
      (loop for box-col from 0 below size do
        (loop with founds = nil
              for row from 0 below size do
                (loop for col from 0 below size do
                  (let ((entry (aref (sudoku-main sudoku)
                                     (+ row (* box-row 3))
                                     (+ col (* box-col 3)))))
                    (if (not (member entry founds))
                        (push entry founds)
                        (progn
                          (format t "Error found at (~A ~A)" row col)
                          (print-sudoku sudoku)
                          (return-from test-boxs nil)))
                    (when (valid-entry entry (sudoku-size sudoku))
                      (incf count)))))))
    (= count (expt size 4))))

(defun location->box (sudoku location)
  "Take in a location and return the box containing it"
  (let* ((box-size (sudoku-box-size sudoku))
         (row (first location))
         (col (second location)))
    (floor (+ (* (floor row box-size) box-size)
              (floor col box-size)))))

(defun new-sudoku (size)
  "Initialize a sudoku with sides being of size. Tradional sudoku is 9 width,
  so the input here would be (new-solver 9)."
  (let ((box-size (sqrt size)))
    (if (not (zerop (mod box-size 1)))
        (error "Size must be a square.")
        (let ((new (make-solver :main (make-array (list size size)
                                                  :element-type 'fixnum
                                                  :initial-element 0)
                                :size size
                                :box-size (floor box-size))))
          new))))

(defun new-solver (size)
  "Initialize a solver with sides being of size. Tradional sudoku is 9 width,
  so the input here would be (new-solver 9)."
  (let ((box-size (sqrt size)))
    (if (not (zerop (mod box-size 1)))
        (error "Size must be a square.")
        (let ((new (make-solver :main (make-array (list size size)
                                                  :element-type 'fixnum
                                                  :initial-element 0)
                                :size size
                                :rows (make-array size)
                                :cols (make-array size)
                                :boxs (make-array size)
                                :box-size (floor box-size))))
          new))))

(defun load-solver (path)
  "Load a puzzle from disk and return it."
  (with-open-file (input path :direction :input)
    (let* ((in-as-string (read-line input))
           (size (length in-as-string))
           (solver (new-solver (floor (sqrt size)))))
      (loop for entry across in-as-string
            for index from 0 below size
            while entry do
              (cond ((digit-char-p entry)
                     (setf (row-major-aref (sudoku-main solver) index)
                           (digit-char-p entry)))
                    ((char= #\. entry)
                     (setf (row-major-aref (sudoku-main solver) index) 0))
                    (t (error "Malformed entries at path during load."))))
      solver)))

(defun set-entry-list (sudoku alist)
  "Set the main sudoku with the entries at the given (location . entry) pairs
  in the list. Destructive."
  (loop for pair in alist do
    (let* ((location (first pair))
           (entry (rest pair))
           (row (first location))
           (col (second location)))
      (set-entry sudoku (list row col) entry)))
  sudoku)

(defun update-vec-with-list (solver list)
  "Destructively increments the entry of the vector of the given locations
  in the list."
  (loop for location in list do
    (incf (svref (solver-rows solver) (first location)))
    (incf (svref (solver-cols solver) (second location)))
    (incf (svref (solver-boxs solver)
                 (location->box (sudoku-main solver) location)))))

(defun scan-for-updates (solver)
  "Check the solver to see if anything in the vectors needs to be updated,
  and if so, update them."
  (let ((found-changes 0))
    (loop for index from 0 below (sudoku-size solver) do
      (let ((old-rows (svref (solver-rows solver) index))
            (old-cols (svref (solver-cols solver) index))
            (old-boxs (svref (solver-boxs solver) index))
            (new-rows (row-count solver index))
            (new-cols (col-count solver index))
            (new-boxs (box-count solver index)))
        (when (/= old-rows new-rows)
          (setf (svref (solver-rows solver) index) new-rows)
          (incf found-changes (- new-rows old-rows)))
        (when (/= old-cols new-cols)
          (setf (svref (solver-cols solver) index) new-cols)
          (incf found-changes (- new-cols old-cols)))
        (when  (/= old-boxs new-boxs)
          (setf (svref (solver-boxs solver) index) new-boxs)
          (incf found-changes (- new-boxs old-boxs)))))
    (values solver (floor found-changes (solver-box-size solver)))))

(defun row-count (sudoku row)
  "How many entries are in this row?"
  (loop for col from 0 below (sudoku-size sudoku)
        counting (valid-entry (aref (sudoku-main sudoku) row col)
                              (sudoku-size sudoku))))

(defun col-count (sudoku col)
  "How many entries are in this column?"
  (loop for row from 0 below (sudoku-size sudoku)
        counting (valid-entry (aref (sudoku-main sudoku) row col)
                              (sudoku-size sudoku))))

(defun box-count (sudoku box)
  "How many entries are in this box?"
  (let ((size (sudoku-box-size sudoku))
        (count 0))
    (multiple-value-bind (box-row box-col)
        (floor box size)
      (loop for row from 0 below size do
        (loop for col from 0 below size
              when (valid-entry (aref (sudoku-main sudoku)
                                      (+ row (* (floor box-row) 3))
                                      (+ col (* (floor box-col) 3)))
                                (sudoku-size sudoku))
                do (incf count))))
    count))

(defun get-entry (sudoku location)
  (aref (sudoku-main sudoku) (first location) (second location)))

(defun set-entry (sudoku location entry)
  (setf (aref (sudoku-main sudoku) (first location) (second location)) entry)
  sudoku)

(defun print-sudoku (sudoku &optional (stream t) depth)
  "Print the puzzle in a human-friendly format."
  (declare (ignore depth))
  (format stream "~%Sudoku:~%")
  (let* ((size (sudoku-size sudoku))
         (box-size (sudoku-box-size sudoku)))
    (loop for row from 0 to size do
      (when (zerop (mod row box-size))
        (format stream "~v@{~A~:*~}~%" (+ (* (+ box-size size) 2) 1) "-"))
      (unless (= row size)
        (loop for col from 0 to size do
          (when (zerop (mod col box-size))
            (if (zerop col)
                (format stream "|")
                (format stream " |")))
          (unless (= col size)
            (let ((entry (get-entry sudoku (list row col))))
              (format stream " ~A" (if (zerop entry)
                                       "."
                                       entry)))))
        (format stream "~%")))))

(defun print-solver (solver &optional (stream t) depth)
  "Print the vectors of the solver in a human-friendly format."
  (declare (ignore depth))
  (print-sudoku solver stream)
  (format stream "Rows:~%~{~A~^ ~}~%" (coerce (solver-rows solver) 'list))
  (format stream "Cols:~%~{~A~^ ~}~%" (coerce (solver-cols solver) 'list))
  (format stream "Boxs:~%~{~A~^ ~}~%" (coerce (solver-boxs solver) 'list)))

(defun solve-but-ones (solver)
  "Keep finding but-one solutions and updating the main solver until none
  exist."
  (let ((found-changes 0)
        (more-changes? t))
    (loop while more-changes?  do
      (multiple-value-bind (entry row col)
          (but-one (scan-for-updates solver))
        (if (null entry)
            (setf more-changes? nil)
            (progn (set-entry solver (list row col) entry)
                   (incf found-changes)
                   (format t "    Solved entry ~A at (~A ~A)~%"
                           entry row col)))))
    (values solver found-changes)))

(defun but-one (solver)
  "Check the vectors to see if any have only one entry remaining, if so return
  the location of it in the main puzzle and the entry to set in the normal alist
  format, otherwise return nil."
  (let ((one-under-max (- (sudoku-size solver) 1)))
    (cond ((find one-under-max (solver-rows solver))
           (multiple-value-bind (row col)
               (locate-but-one-row solver)
             (let ((entry (value-but-one solver row col)))
               (RETURN-FROM but-one (values entry row col)))))
          ((find one-under-max (solver-cols solver))
           (multiple-value-bind (row col)
               (locate-but-one-col solver)
             (let ((entry (value-but-one solver row col)))
               (RETURN-FROM but-one (values entry row col)))))
          ((find one-under-max (solver-boxs solver))
           (multiple-value-bind (row col)
               (locate-but-one-box solver)
             (let ((entry (value-but-one solver row col)))
               (RETURN-FROM but-one (values entry row col)))))
          (t nil))))

(defun value-but-one (solver row col)
  "Take a solver and an empty location with either a row, column, or box
  missing only that location from being filled, and return the entry that
  fills it."
  (let* ((row-contents (row-contents solver row))
         (col-contents (col-contents solver col))
         (box-contents (box-contents solver (location->box
                                             solver (list row col))))
         (valids (get-valid-entries solver))
         (target-size (- (length valids) 1)))
    (cond ((= target-size (length row-contents))
           (first (set-difference valids row-contents)))
          ((= target-size (length col-contents))
           (first (set-difference valids col-contents)))
          ((= target-size (length box-contents))
           (first (set-difference valids box-contents)))
          (t (error "Value-but-one receivied an invalid location")))))

(defun locate-but-one-row (solver)
  "We know there is a row with size-1 entries. Locate it and return the
  location of the empty entry."
  (let* ((row (position (- (sudoku-size solver) 1) (solver-rows solver)))
         (row-contents (array-row (sudoku-main solver) row))
         (col (position 0 row-contents)))
    (values row col)))

(defun locate-but-one-col (solver)
  "We know there is a column with size-1 entries. Locate it and return
  location of the empty entry."
  (let* ((col (position (- (sudoku-size solver) 1) (solver-cols solver)))
         (col-contents (array-col (sudoku-main solver) col))
         (row (position 0 col-contents)))
    (values row col)))

(defun locate-but-one-box (solver)
  "We know there is a box with size-1 entries. Locate it and return
  location of the empty entry."
  (let ((box (position (- (sudoku-size solver) 1) (solver-boxs solver))))
    (multiple-value-bind (row-min row-max col-min col-max)
        (box-range solver box)
      (loop for row from row-min to row-max do
        (loop for col from col-min to col-max do
          (when (zerop (get-entry solver (list row col)))
            (RETURN-FROM locate-but-one-box (values row col)))))))) 

(defun solve-no-choice (solver)
  "Check that all the single possibilites in the possibilities array are
  present as solutions in the solver. If not, add them."
  (let ((found-changes 0))
    (let ((possibilities (make-possibilities-array solver)))
      (loop for row from 0 below (sudoku-size solver) do
        (loop for col from 0 below (sudoku-size solver) do
          (let ((entries (aref possibilities row col)))
            (when (and (zerop (get-entry solver (list row col)))
                       (= (length entries) 1))
              (set-entry solver (list row col) (first entries))
              (incf found-changes)
              (format t "    Solved entry ~A at (~A ~A)~%"
                      (first entries) row col))))))
    (values solver found-changes)))

(defun remove-impossibilities (solver array)
  "If a box contains a row or column with some specific entry as a
  possibility, and that is not possible anywhere else then other boxes
  sharing that row or column cannot contain that entry. Remove them from
  the possibilities list"
  (loop for box from 0 below (solver-size solver) do
    (multiple-value-bind (row-min row-max col-min col-max)
        (box-range solver box)
      (loop for col from col-min to col-max do
        (let ((entries (array-col-uniques array col
                                          row-min row-max col-min col-max)))
          (loop for row from 0 below (solver-size solver) do
            (unless (and (<= row row-max)
                         (>= row row-min))
              (let ((p-entries (aref array row col)))
                (when (intersection entries p-entries)
                  (setf (aref array row col)
                        (remove-list entries p-entries))))))))
      (loop for row from row-min to row-max do
        (let ((entries (array-row-uniques array row
                                          row-min row-max col-min col-max)))
          (loop for col from 0 below (solver-size solver) do
            (unless (and (<= col col-max)
                         (>= col col-min))
              (let ((p-entries (aref array row col)))
                (when (intersection entries p-entries)
                  (setf (aref array row col)
                        (remove-list entries p-entries))))))))))
  array)

(defun make-possibilities-array (solver)
  "Build an array of lists containing the entries that are valid at each
  location. May contain impossible entries but will never miss entries that
  are possible."
  (let* ((size (sudoku-size solver))
         (possibilities (make-array (list size size)))
         (valid-list (loop for valid from 1 to size collecting valid)))
    (loop for entry-row from 0 below size do
      (loop for entry-col from 0 below size do
        (let ((entry (get-entry solver (list entry-row entry-col))))
          (setf (aref possibilities entry-row entry-col)
                (if (not (zerop entry))
                    (list entry)
                    (let (rem-list)
                      (loop for row-rem in (row-contents solver entry-row) do
                        (setf rem-list (adjoin row-rem rem-list)))
                      (loop for col-rem in (col-contents solver entry-col) do
                        (setf rem-list (adjoin col-rem rem-list)))
                      (loop
                        for box-rem
                          in (box-contents solver
                                           (location->box solver
                                                          (list entry-row
                                                                entry-col)))
                        do (setf rem-list (adjoin box-rem rem-list)))
                      (remove-list rem-list valid-list)))))))
    (remove-impossibilities solver possibilities)))

(defun row-contents (solver row)
  "What are the contents of this row?"
  (let (result)
    (loop for col from 0 below (sudoku-size solver) do
      (let ((entry (aref (sudoku-main solver) row col)))
        (when (not (zerop entry))
          (push entry result))))
    (sort result #'<)))

(defun col-contents (solver col)
  "What are the contents of this column?"
  (let (result)
    (loop for row from 0 below (sudoku-size solver) do
      (let ((entry (aref (sudoku-main solver) row col)))
        (when (not (zerop entry))
          (push entry result))))
    (sort result #'<)))

(defun box-contents (solver box)
  "What are the contents of this box?"
  (let (result)
    (multiple-value-bind (row-min row-max col-min col-max)
        (box-range solver box)
      (loop for row from row-min to row-max do
        (loop for col from col-min to col-max do
          (let ((entry (aref (sudoku-main solver) row col)))
            (when (not (zerop entry))
              (push entry result))))))
    (sort result #'<)))

(defun box-range (solver box)
  "What is the extent of the rows and columns that the box covers?"
  (let* ((box-size (sudoku-box-size solver))
         (box-row (floor box box-size))
         (box-row-min (* box-row box-size))
         (box-row-max (- (+ box-row-min box-size) 1))
         (box-col (mod box box-size))
         (box-col-min (* box-col box-size))
         (box-col-max (- (+ box-col-min box-size) 1)))
    (values box-row-min box-row-max
            box-col-min box-col-max)))

(defun get-valid-entries (sudoku)
  "What are the possible entries in this puzzle?"
  (loop for entry from 1 to (solver-size sudoku) collecting entry))

(defun solve-forced-over (solver)
  "Check the possibilities to see if any entries can be forced due to other
  rows/cols/boxs already containing that entry. Add them if they exist. Return
  the solver."
  (let ((possibles (make-possibilities-array solver))
        (found-changes 0))
    (loop for box from 0 below (sudoku-size solver) do
      (let (box-possible-list)
        (multiple-value-bind (row-min row-max col-min col-max)
            (box-range solver box)
          (loop for row from row-min to row-max do
            (loop for col from col-min to col-max do
              (when (zerop (get-entry solver (list row col)))
                (loop for poss in (aref possibles row col) do
                  (if (assoc poss box-possible-list)
                      (incf (second (assoc poss box-possible-list)))
                      (push (list poss 1 row col) box-possible-list))))))
          (loop for possible in box-possible-list do
            (when (= (second possible) 1)
              ;; (format t "Row: ~A~%Col: ~A~%Entry: ~A~%" (third possible)
              ;;            (fourth possible) (first possible))
              (let ((row (third possible))
                    (col (fourth possible))
                    (entry (first possible)))
                (set-entry solver (list row col) entry)
                (incf found-changes)
                (format t "    Solved entry ~A at (~A ~A)~%"
                        entry row col)))))))
    (values solver found-changes)))

(defun solve-by-guessing (solver)
  "Pick the easiest entries to solve, see if they quickly lead to a complete
  solution. If they do, push it to the main puzzle. If not move on to the next.
  Stores the guesses in a hashmap according to how many entries remain unsolved.
  If any guess has zero then its the solution."
  (let ((guesses (make-hash-table))
        (possibilities (make-possibilities-array solver))
        (priorities (make-priority-queue solver))
        (count-goal (expt (solver-size solver) 2))
        (attempts 0))
    (loop for location in priorities do
      (let ((row (first location))
            (col (second location)))
        (loop for entry in (aref possibilities row col) do
          (incf attempts)
          (format t "~%~%--- Guessing ~A at (~A ~A)~%" entry row col)
          (let* ((guess (deterministic-loop
                         (set-entry (duplicate-solver solver)
                                    location entry)))
                 (count (count-entries guess)))
            (format t "--- Tested placing ~A in (~A ~A)~%" entry row col)
            (format t "--- Found ~A entries out of ~A~%~%" count count-goal)
            (setf (gethash (list count location entry) guesses)
                  guess)
            (when (and (= count count-goal)
                       (solvedp guess))
              (RETURN-FROM solve-by-guessing (values guess attempts)))))))))

(defun count-entries (solver)
  (let ((rows (sum-vector (solver-rows solver)))
        (cols (sum-vector (solver-cols solver)))
        (boxs (sum-vector (solver-boxs solver))))
    (if (= rows cols boxs)
        rows
        (error "Rows Cols and Boxs didn't add up."))))

(defun make-priority-queue (solver)
  "Organize the entries by easiest to solve. Returns an list
  of (count row col). Starts with entries with the fewest possibilities."
  (let ((possibilities (make-possibilities-array solver))
        (priorities nil)
        (size (sudoku-size solver)))
    (loop for row from 0 below size do
      (loop for col from 0 below size do
        (let ((length (length (aref possibilities row col))))
          (when (> length 1)
            (push (list length row col) priorities)))))
    (mapcar #'(lambda (list)
                (list (second list) (third list)))
            (sort priorities #'(lambda (lista listb)
                                 (< (first lista) (first listb)))))))

(defun guess (solver entry row col)
  "If the location is entry, is the puzzle solved by non-guess methods? Return
  puzzle based on the guess with all non-determinitive solutions adjusted."
  (let ((guess (duplicate-solver solver)))
    (main-loop (set-entry guess (list row col) entry))))

(defun duplicate-solver (solver)
  "Make a new solver that doesn't share anything with the old."
  (let* ((size (sudoku-size solver))
         (new (new-solver size)))
    (loop for index from 0 below (expt size 2) do
      (setf (row-major-aref (sudoku-main new) index)
            (row-major-aref (sudoku-main solver) index)))
    (loop for index from 0 below size do
      (setf (svref (solver-rows new) index)
            (svref (solver-rows solver) index)
            (svref (solver-cols new) index)
            (svref (solver-cols solver) index)
            (svref (solver-boxs new) index)
            (svref (solver-boxs solver) index)))
    new))

(defun deterministic-loop (solver)
  "This is the main solver loop. Takes in a solver and returns the puzzle with
  as many entries solved as it can."
  (loop with updated-main = t
        for main-steps from 0
        until (and (null updated-main)
                   (plusp main-steps))
        do (let ((total-updates 0))
             ;; But-ones
             (multiple-value-bind (solver updates)
                 (solve-but-ones solver)
               (declare (ignore solver))
               (incf total-updates updates)
               (format t "  - Found ~A but-ones~%" updates))
             ;; No-coices
             (multiple-value-bind (solver updates)
                 (solve-no-choice solver)
               (declare (ignore solver))
               (incf total-updates updates)
               (format t "  - Found ~A no-choices~%" updates))
             ;; Forced-overs
             (multiple-value-bind (solver updates)
                 (solve-forced-over solver)
               (declare (ignore solver))
               (incf total-updates updates)
               (format t "  - Found ~A forced-overs~%" updates))
             ;; Update the vectors and print final update count
             (scan-for-updates solver)
             (when (zerop total-updates)
               (setf updated-main nil))
             (format t " -- Found ~A total updates in step ~A~%"
                     total-updates main-steps)))
  solver)

(defun non-deterministic-loop (solver)
  "Secondary loop takes the solver and processes it using other means such as
  guess-and-check."
  (format t "~%~%*** Starting non-deterministic steps~%~%")
  (multiple-value-bind (solver-bg attempts-bg)
      (solve-by-guessing solver)
    (format t "--- Took ~A attempts to guess~%" attempts-bg)
    solver-bg))

(defun main-loop (solver)
  "Take the solver and process it. Run the deteministic loop first, then pass
  the results to the non-deterministic functions. Return the completed solver."
  (print-sudoku solver)
  (format t "~%~%*** Starting determinitstic steps~%~%")
  (non-deterministic-loop (deterministic-loop solver)))

;; New puzzles require initialization befoe running the main loop
(defun run-loop ()
  "Run this to load in a problem at path, and solve it. Currently returns the
  solution. Will write the solution to disk too eventually. Returns the
  completed solver."
  (main-loop (scan-for-updates (load-test))))

;; Set this to the path of the file to run on
(defun load-test ()
  "Load the file from disk."
  (load-solver "~/common-lisp/sudoku/src/puzzle.csv"))









