(defsystem "sudoku"
  :version "0.0.0"
  :author "Michael Reis"
  :license "AGPL3"
  :depends-on ("mtreis-scripts")
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "sudoku"))))
  :description "Sudoku generator and solver"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "sudoku/tests"))))

(defsystem "sudoku/tests"
  :author "Michael Reis"
  :license "AGPL3"
  :depends-on ("sudoku"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for sudoku"

  :perform (test-op (op c) (symbol-call :rove :run c)))
